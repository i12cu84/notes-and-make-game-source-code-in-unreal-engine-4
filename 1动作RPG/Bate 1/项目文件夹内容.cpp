项目内文件夹对应的内容
以下为文件夹所对应的内容描述
Abilities - 能力 文件夹
    DataTables - 相关攻击数据
        AttackDamage - 曲线表格 角色榔头武器造成的伤害
        AttackDamage_Hammer - 曲线表格 角色锤子武器造成的伤害
        AttackDamage_Sword - 曲线表格 角色刀剑武器造成的伤害
        StartingStats - 曲线表格 角色基础属性(移动 血量等)
    Enemies - 敌人的能力 文件夹
        Goblin - 哥布林能力 文件夹
            GA_GoblinMelee - 挥砍动画 蓝图类
            GA_GoblinRange01 - 喷毒气动画 蓝图类
            GE_GoblinMelee - 后三应该是伤害 数据 蓝图类
            GE_GoblinRange - 蓝图类
            GE_GoblinStats - 蓝图类
        Spider - 蜘蛛能力 文件夹
            GA_SpiderCharge - 遁地动画 蓝图类
            GA_SpiderFirewall - 叫唤动画 蓝图类
            GA_SpiderMelee - 蓝图类
            GE_FirewaveDamage - 蓝图类
            GE_SpiderCharge - 蓝图类
            GE_SpiderStats - 蓝图类
            TargetType_Firewall - 蓝图类
    Player - 玩家能力 文件夹
        Axe - 斧头能力 文件夹
            GA_PlayerAxeMelee - 普通攻击挥砍 蓝图类
            GE_PlayerAxeBurstPound - 普通攻击数据 蓝图类
            GE_PlayerAxeGroundPound
            GE_PlayerAxeMelee
            TargetType_BurstPound
            TargetTyoe_GroundPound
        Hammer - 榔头能力 文件夹
            GA_PlayerHammerMelee
            GE_PlayerHammerBurstPound
            GE_PlayerHammerGroundPound
            GE_PlayerHammerMelee
            TargetType_HammerBurstPound
            TargetType_HammerGroundPound
        Potions - 喝药能力 文件夹
            GA_DeathsDoor
            GA_PotionHealth
            GA_PotionMana
            GE_DeathsDoor
            GE_PotionHealth
            GE_PotionMana
        Skills - 获取能力 文件夹
            BP_Fireball - 获取actor 蓝图类 (蓝图!!!!!!!!!!!!!!!)
            BP_SlimeBall - 获取actor 蓝图类 (蓝图!!!!!!!!!!!!!!!)
            GA_PlayerSkillFireball - 获得怒气 蓝图类
            GA_PlayerSkillFireWave - 释放怒气 蓝图类
            GA_PlayerSkillMeteor - 技能滚雷 蓝图类
            GA_PlayerSkillmeteorStorm - 技能阵地 蓝图类
            GE_PlayerSkillCooldowm - 后几个相关数据 蓝图类
            GE_PlayerSkillFireball
            GE_PlayerSkillFireWave
            GE_PlayerSkillFireManaCost
            GE_PlayerSkillFireMeteor
            GE_PlayerSkillFireMeteorStorm
            TargetType_FireWave
            TargetType_Meteor
            TargetType_MeteorStorm
        Sword - 刀剑能力 文件夹
            GA_PlayerSwordMelee - 普通挥砍 蓝图类
            GA_PlayerSwordChestKick - 刀剑的相关数据
            GA_PlayerSwordFrontalAttack
            GA_PlayerSwordJumpSlam
            GA_PlayerSwordMelee
            TargetType_ChestKick
            TargetType_FrontalAttack
            TargetType_JumpSlam
    Shared - 文件夹
        BP_AbilityProjectileBase - actor事件重叠 蓝图类 (蓝图!!!!!!!!!!!!!)
        GA_AbilityBase - (蓝图!!!!!!!!!!!!!)
        GA_MeleeBase - (蓝图!!!!!!!!!!!!!)
        GA_PotionBase - (蓝图!!!!!!!!!!!!!)
        GA_SkillBase - (蓝图!!!!!!!!!!!!!)
        GA_SpawnProjectileBase - (蓝图!!!!!!!!!!!!!)
        GE_Damagelmmune
        GE_GodMode
        GE_HealBase
        GE_MeleeBase
        GE_RangedBase
        GE_StatsBase
        TargetType_SphereTrace - (蓝图!!!!!!!!!!!!!)
Animations - 动画 文件夹
    Boss - Boss动画 文件夹
        AM_Spider_Charge - 遁地动画
        AM_Spider_Firewall - 交换动画
        AM_Spider_Hit - 受到伤害动画
        AM_Spider_Melee - 普通攻击动画
        AM_Spider_Spawn - 跳跃攻击动画
    NPC - NPC动画 文件夹
        AM_Guardian_Attack - 挥砍动画
        AM_Guardian_Attack02 - 喷毒气动画
        AM_Guardian_Falldown - 阵亡动画
        AM_Guardian_Hit - 被击动画
Assets - 素材文件夹
    Sounds - 声音素材(内容略)
Blueprints - AI蓝图文件夹
    AI - AI 文件夹
        BossAI - BOSSAI相关 文件夹
            AIC_Boss - AI控制
            BT_Boss - 行为树
        GruntAI - 哥布林AI相关 文件夹
            AIC_NPC
            AIC_NPC_Range - 随机中断
            BT_NPC
            EQ_FindPlayer - Querier范围生成怪物
        PlayerAI
            AIC_Player
            BT_Player
        
        BB_Base - 黑板数据
        BTDec_CheckHealth - (全是蓝图!)
        BTDec_CheckItem
        BTDec_ConeCheck
        BTDec_IsAlive - 存活蓝图 (什么都不做)
        BTService_DistanceToTarget
        BTService_FindNearestTarget
        BTService_RandomMoveSpeed
        BTTask_AttackMelee
        BTTask_AttackSkill
        BTTask_GoAroundTarget
        BTTask_IsPlayingHighPriorityMontage
        BTTask_IsTargetSurrounded
        BTTask_StopAndRotate
        BTTask_StopAttack
        BTTask_UseAbility
        BTTask_UseItem
        EQS_PlayerContext

    AnimNotifies - 场景中的蓝图(? 目前未详解)
        AN_MaterialEffect
        AN_Visibility
        BlockMoveInputNS
        CameraShakeNotify
        Death_Notify
        Hit_Notify
        JumpSectionNS
        MoveForwardNotify
        MoveForwardNS
        RangeAttackNS
        ShieldNS
        SlomoNotify
        SlomoNS
        StopAndStartAI_NS
        UseItemNS
        WeaponAttackNS
    Boss - Boss的相关动作动画蓝图 文件夹
        ABP_SpiderBoss - boss动画蓝图
        BS_SpiderBoss - boss动画
        NPC_SpiderBoss - boss蓝图行为
    CameraShake - 镜头混动数据 文件夹
        BP_Camerashake - 蓝图默认震荡
        camera_shake_Skill - 技能镜头震荡
        camera_shake_small - 普通攻击镜头震荡
    NPC - 哥布林动画蓝图 文件夹
        Idle_Run_Guardian_1D - 哥布林静止动画
        NPC_AnimBP_Base - 哥布林阵亡动画 
        NPC_Goblin_Level_01 - 哥布林等级参数 下同
        NPC_Goblin_Level_02
        NPC_Goblin_Level_03
        NPC_GOblinBP - 哥布林行为蓝图
        NPC_SpawnBox - 哥布林出生点
    Progression
        SpawnGroupStruct
        WavesProgression
        WaveStruct - 数据结构
    SaveGame
        GlobalOptionsSaveGame
        GlobalOptionsStruct
    Weapon - 武器 文件夹
        Goblin - 哥布林武器数据 文件夹
            GoblinWeapon_Axe
            GoblinWeapon_Base
            GoblinWeapon_Torch
        Spider - 蜘蛛 文件夹
            BP_WeaponSpider - 蜘蛛BP发射器 蓝图类

        BP_Waepon ......等 - 武器BP模型
        WaeponActor - 武器碰撞蓝图 蓝图类

    WidgetBP- 游戏界面UI
        HUD
            iHUD
            WB_HUD_Mobile - 手机UI界面
            WB_HUD_PC - 电脑UI界面
            WB_InputButton - 普通攻击按钮
            WB_InputLabel - 空格按钮
            WB_OnScreenInput - 手机UI攻击按钮
        Inventory
            WE_Equipment - 5*Item
            WB_EquipmentSlot - Item
            WB_InventoryItem - 雷锤
            WB_InventoryList - 列表
            WB_Purchase - 物品栏使用(?)
            WB_PurchaseItem - 雷锤

        CommonWords - 存储关键帧
        Debug_UI - start cmds
        WB_ButtonText - 文本按钮
        WB_Checkbox
        WB_DamageNumber - 倒计时时间
        WB_EnemyHP - 敌人血条
        WB_GenericButton
        WB_HUD_Pickup - Text Block
        WB_InGame_Finish - GameOver界面
        WB_OptionsScreen - options设置界面
        WB_PauseMenu - 返回设置菜单按钮
        WB_PointsLabel - souls标签
        WB_SelectLanguage - 语言设置选择
        WB_Skipintro - 加速/快进按钮(?)
        WB_Target - 副选按钮
        WB_Title - 游戏开始界面按钮
        WB_WaveEnd - wave complete 
        WB_WaveStart - wave1
        WC_DamageText - 蓝图

    BP_Character - 
    BP_EnemyCharacter
    BP_GameInstance
    BP_GameMode - 游戏模式
    BP_GameState
    BP_MainMenuGameMode
    BP_PlayerCharacter - 主角角色
    BP_PlayerController
    BP_RPGFunctionLibrary
    BP_SoulItem
    BP_SpectatorPawn
Characters - 角色 文件夹
    Animations - 角色动画 文件夹
    Barbarous - 角色材质 文件夹
        Texture_Materials - 材质 文件夹 
            略

    ABP_Player - 角色动画蓝图
    BS_Sword_Idle_Run - 角色混合空间
    PA_Mannequin_Physics - 角色物理资产
    SK_Mannequin_Skeleton - 角色骨骼
Effects - 特效 文件夹
    略
Enemy - 敌人 文件夹
    略
Environment - 环境 文件夹
    略
Items - 文件夹
    略
Maps - 地图 文件夹
    略
Sequences - 文件夹
    略
UI - UI 文件夹
    略
Weapons - 武器 文件夹
    略

0Clos - (自设计 换装系统文件夹)
    BP_GameMode - 换装界面的游戏mode
    CustomCharacter - Actor展示
    CustomMap - 换装的关卡
    CustomPlayerController - 换装的控制逻辑
    E_part - 当前换装项目的枚举
    MainWiget - 换装UI
    MyGameInstance - 存储数据蓝图
    NewWorld 设定的闯关关卡(正在设计)
    S_SavedParts换装数据表格

0Bags - (自设计 背包系统)
    BP_Inventory - 背包系统蓝图
    BP_ItemHP - 背包物品的蓝图类 HP
    BP_ItemMP - 背包物品的蓝图类 MP
    BP_ItemRenWu - 背包物品的蓝图类  任务物品
    BP_ItemShouShi - 背包物品的蓝图类 首饰
    BP_ItemWuQi - 背包物品的蓝图类 武器
    BP_ItemYaoDai - 背包物品的蓝图类 腰带
    BP_ItemMasterItem - 背包中物品的模板父类(上面背包物品的父类)
    Button - 按钮素材
    DragInventory - 关键蓝图模板
    DragSlot - 关键蓝图模板 拖动效果BP
    E_ItemType - 物品枚举类
    ItemHP - 物品素材 HP
    ItemMP - 物品素材 MP
    ItemRenWu - 物品素材 任务物品
    ItemShouShi - 物品素材 首饰
    ItemWuQi - 物品素材 武器
    ItemYaoDai - 物品素材 腰带
    S_Item - 物品结构体
    S_Slot - 背包结构体
    Text_Map - 背包系统测试地图
    TsetItem - 物品在地图上的实例化Actor
    W_Inventory - 背包UI及物品应用UI
    W_Item_Decription
    W_MainWidget - 丢弃UI及背包应用错误UI
    W_Slot - 物品叠加后数量贴图UI
    W_ThrowMenu - 物品丢弃UI
    W_UseageMenu - 物品应用UI
    
0SCK_Casual01 - ue4同名换装系统素材(来源虚幻商城)
    略